<?php
require __DIR__ . '/vendor/autoload.php';
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\CssSelector\CssSelectorConverter;

//BEGIN html opvragen van de gegeven url
function curl($url) {

    $options = Array(
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_FOLLOWLOCATION => TRUE,
        CURLOPT_AUTOREFERER => TRUE,
        CURLOPT_HEADER=> TRUE,
        CURLOPT_CONNECTTIMEOUT => 1200,
        CURLOPT_TIMEOUT => 1200,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_USERAGENT => "Googlebot/2.1 (+http://www.googlebot.com/bot.html)",
        CURLOPT_URL => $url,
        CURLOPT_ENCODING=>'gzip,deflate',
    );

    $ch = curl_init();
    curl_setopt_array($ch, $options);

    $data = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if($httpcode!="200")
    {
        return "error";
    }

    return $data;
}
//END html opvragen van de gegeven url



//BEGIN de viabovag.nl website Crawlen om hier de gewenste gegevens vandaan te kunnen halen
function crawlBovag($html)
{
    $crawler = new Crawler($html);
    $allprices = 0;
    $nodeValues = $crawler->filter('p.search-result__vehicle-price')->each(function (Crawler $node, $i) {

        if (null !== $node->text()) {
            $prices = $node->text();
            $prices = preg_replace('/^(-)|[^0-9,]+/', '', $prices);
            $prices = (int)$prices;
            return $prices;
        }
    });
    if (array_sum($nodeValues) > 0) {
        return array_sum($nodeValues) / count($nodeValues);
    }
    else {
        return null;
    }
    return array_sum($nodeValues) / count($nodeValues);
}
//END de viabovag.nl website Crawlen om hier de gewenste gegevens vandaan te kunnen halen


//BEGIN informatie verkrijgen van het RDW via het ingevoerde kenteken
function getRDW(){
    $kentekenInput = returnKenteken($_GET['kenteken']);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://opendata.rdw.nl/resource/m9d7-ebf2.json?kenteken=' . $kentekenInput);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        '$$app_token:hJdBzMR5t2Bi4MNLJMKpYHJwP'
    ));

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);
    $final = json_decode($result);
    return $final;
}
//END informatie verkrijgen van het RDW via het ingevoerde kenteken

//BEGIN Via de viabovag.nl website de afbeelding ophalen van de eerste auto op de webpagina
function image($html){
    $crawler = new Crawler($html);
    $allprices = 0;
    $nodeValues = $crawler->filter('img.search-result__image')->first()->attr('src');
    return $nodeValues;
}
//END Via de viabovag.nl website de afbeelding ophalen van de eerste auto op de webpagina

//BEGIN de link van de viabovag.nl auto ophalen
function piclink($html){
    $crawler = new Crawler($html);
    $allprices = 0;
    $nodeValues = $crawler->filter('a.search-result__vehicle-section')->first()->attr('href');
    return $nodeValues;
}
//END de link van de viabovag.nl auto ophalen

//BEGIN Het ingevoerde kenteken omzetten naar het formaat dat de RDW vraagt
function returnKenteken($licenseplate){
    return strtoupper(preg_replace("/[^a-zA-Z0-9\s]/", "", $licenseplate));
}
//END Het ingevoerde kenteken omzetten naar het formaat dat de RDW vraagt
?>
