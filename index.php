<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>KentekenChecker</title>

    <!-- Includes and Requires -->
    <?php
    include 'functions.php';
    require __DIR__ . '/vendor/autoload.php';
    ?>
    <!-- Includes and Requires -->

    <!-- css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/coliff/bootstrap-rfs/bootstrap-rfs.css">
    <!-- css -->
</head>

<body class="bg-dark">

    <?php
    if (isset($_GET['kenteken'])) {
        //BEGIN De data verkregen van de RDW in een variabel stoppen
        $final = getRDW();
        //END Het ingevoerde kenteken formatten naar de manier hoe het RDW het graag ontvangt
        $kentekenInput = returnKenteken($_GET['kenteken']);
    }
    ?>

    <div class="container-fluid">
        <?php

        // Wanneer er een kenteken is ingevoerd en de lengte is goed. En er is informatie van het RDW opgehaald voer je de volgende code uit:
        if (isset($_GET['kenteken']) && strlen($kentekenInput) === 6 && count($final) !== 0) {

            //BEGIN Het model van het voertuig minimaal maken zodat het opgezocht kan worden via viabovag.nl
            $handelsbenaming = strtolower(preg_replace('/(\b[-][a-zA-Z]{0,3}\b)$/','' ,$final[0]->handelsbenaming));
            if (str_contains($handelsbenaming, strtolower($final[0]->merk))) {
                $handelsbenaming = str_replace(strtolower($final[0]->merk), '', $handelsbenaming);
            }
            $handelsbenaming = strtolower(preg_replace('/((?:[a-zA-Z]+[0-9]|[0-9]+[a-zA-Z])[a-zA-Z0-9]*)/','' ,$handelsbenaming));
            $handelsbenaming = strtolower(preg_replace('/\b[a-z]{1,2}\b/','' ,$handelsbenaming));
            $handelsbenaming = preg_replace('/[0-9\.\s\-]+/', '', $handelsbenaming);
            //END Het model van het voertuig minimaal maken zodat het opgezocht kan worden via viabovag.nl

            //BEGIN wanneer het aantal zitplaatsen niet is bijgevoegd in het rdw neemt men deze ook niet mee bij de data van viabovag.nl (Kan nog uitgebreid worden met meerdere cases)
            switch (true) {
                case !isset($final[0]->aantal_zitplaatsen):
                $url = strtolower("https://www.viabovag.nl/auto/" . "merk-" . strtolower($final[0]->merk) . "/model-" . strtolower(preg_replace('/([-][a-zA-Z]{0,3})$/','' ,$final[0]->handelsbenaming)). "/bouwjaar-vanaf-" . substr($final[0]->datum_eerste_toelating, 0, -4) . "/bouwjaar-tot-en-met-" . substr($final[0]->datum_eerste_toelating, 0, -4) . "/");
                break;

                default:
                $url = strtolower("https://www.viabovag.nl/auto/" . "merk-" . strtolower($final[0]->merk) . "/model-" . strtolower(preg_replace('/([-][a-zA-Z]{0,3})$/','' ,$final[0]->handelsbenaming)). "/bouwjaar-vanaf-" . substr($final[0]->datum_eerste_toelating, 0, -4) . "/bouwjaar-tot-en-met-" . substr($final[0]->datum_eerste_toelating, 0, -4) . "/" . "/zitplaatsen-" . $final[0]->aantal_zitplaatsen . "/");
                break;
            }
            //END wanneer het aantal zitplaatsen niet is bijgevoegd in het rdw neemt men deze ook niet mee bij de data van viabovag.nl (Kan nog uitgebreid worden met meerdere cases)

            // Haalt de html op van de aangevraagde url zodat deze verder in de code nog gebruikt kan worden
            $results_page = curl($url);

            // Haalt de gemiddelde prijs op van het aangevraagde voertuig op viabovag.nl
            $full_result = crawlBovag($results_page);
        }
        ?>

        <div class="row">
            <div class="col-lg-8 col-xs-12 offset-xs-0 offset-lg-2">
                <div class="card">
                    <div class="card-header bg-dark text-white">
                        <form class="" action="index.php" method="get">

                            <div class="row">
                                <div class="col-lg-8 offset-md-2 text-center">
                                    <h1 style="font-size:4vh" class="title form-label">Kenteken:</h2>
                                    </div>
                                </div>


                                <div class="form-row needs-validation">
                                    <div class="form-group col-lg-8 mx-auto mb-3">
                                        <div class="input-group has-validation">

                                            <!--BEGIN Hier wordt de zoekopdracht naar het kenteken ingevuld -->
                                            <input type="text" class="form-control <?php if (isset($_GET['kenteken'])) {
                                                // Wanneer er geen informatie is voor het kenteken wordt hij op "is-invalid" gezet
                                                if (!isset($final[0])) {
                                                    echo "is-invalid";
                                                }
                                                else {
                                                    echo "is-valid";
                                                }
                                            } ?>" style="text-align:center; font-size:2vh" name="kenteken" value="<?php if(isset($_GET['kenteken'])){echo $kentekenInput;} ?>">
                                            <!--END Hier wordt de zoekopdracht naar het kenteken ingevuld -->

                                            <!--BEGIN Dit is een melding die wordt weergegeven wanneer de gebruiker een verkeerd kenteken invuld -->
                                            <div class="input-group-append">
                                                <input style="font-size:2vh" class="btn btn-info" type="submit" placeholder="zoeken">
                                            </div>

                                            <div class="invalid-tooltip">U moet een geldig kenteken invullen!</div>
                                            <!--END Dit is een melding die wordt weergegeven wanneer de gebruiker een verkeerd kenteken invuld -->

                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-body">
                            <?php

                            //BEGIN Deze code wordt uitgevoerd wanneer er iets is ingevuld bij het kenteken.
                            if (isset($_GET['kenteken']) && $_GET['kenteken'] !== "" && isset($final[0]))
                            {
                                ?>

                                <!--BEGIN Hier wordt de Gemiddelde prijs op viabovag.nl weergegeven -->
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <p style="font-size:2vh">
                                            <?php if ($full_result !== null){ ?>
                                                Gemiddelde Prijs op viabovag.nl: €<?php echo $full_result; ?>
                                            <?php }
                                            else {
                                                ?>
                                                Dit exemplaar kan niet gevonden worden op viabovag.nl
                                                <?php
                                            } ?>
                                        </p>
                                    </div>
                                </div>
                                <!--END Hier wordt de Gemiddelde prijs op viabovag.nl weergegeven -->


                                <!--BEGIN Hier wordt een afbeelding weergegeven die je redirect naar de shop pagina van de auto in kwestie -->
                                <?php
                                if ($full_result !== null) {?>
                                    <div class="row">
                                        <div class="col-lg-12 text-center">
                                            <a href="https://viabovag.nl<?php echo piclink($results_page); ?>">
                                                <img class="img-fluid" height="40vh" src="<?php echo(image($results_page));?>" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <!--END Hier wordt een afbeelding weergegeven die je redirect naar de shop pagina van de auto in kwestie -->

                                <!--BEGIN Een tabel om de gegevens van het RDW in weer te geven -->
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th style="font-size:2vh" scope="col">Data</th>
                                                <th style="font-size:2vh" scope="col">Value</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($final[0] as $key => $value) {
                                                if (substr( $key, 0, 27 ) !== "api_gekentekende_voertuigen"){

                                                    ?>
                                                    <tr>
                                                        <td style="font-size:2vh"><?php echo ucwords(str_replace("_", " ", $key), ' '); ?></td>
                                                        <td style="font-size:2vh"><?php echo $value; ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!--END Een tabel om de gegevens van het RDW in weer te geven -->

                                <?php
                            }
                            //END Deze code wordt uitgevoerd wanneer er iets is ingevuld bij het kenteken.


                            //BEGIN Wanneer er geen informatie wordt weergegeven wordt er informatie weergegeven die de gebruiker aangeeft dat hij een kenteken in moet vullen
                            else {
                                ?>
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <p>
                                            Wanneer u gezocht heeft naar een geldig kenteken wordt hier de informatie weergegeven die bij het betreffende kenteken hoort.
                                        </p>
                                    </div>
                                </div>
                                <?php
                            }
                            //END Wanneer er geen informatie wordt weergegeven wordt er informatie weergegeven die de gebruiker aangeeft dat hij een kenteken in moet vullen

                            ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </body>
    <!--BEGIN javascript -->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
    <!--END javascript -->
    </html>
